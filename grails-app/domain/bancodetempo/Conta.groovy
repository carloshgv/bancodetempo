package bancodetempo

class Conta {
	int horas = 10
	int minutos = 0
	Date dateCreated 
	Date lastUpdated 

	static belongsTo = [ persoa: Persoa ]
	static hasMany = [ levantamentos: Movemento, ingresos: Movemento ]
	static mappedBy = [ levantamentos: 'levantamento', ingresos: 'ingreso' ]
	static constraints = { 
		horas min: 0
		minutos min: 0, max: 59
	}

	def beforeInsert() {
		if(minutos > 59) {
			horas += Math.floor(minutos / 60)
			minutos = minutos % 60
		}
	}

	def beforeUpdate() {
		if(isDirty('minutos')) {
			if(minutos > 59) {
				horas += Math.floor(minutos / 60)
				minutos = minutos % 60
			}
		}
	}
}
