package bancodetempo

class Persoa {
	transient springSecurityService
	static searchable = {
		only = [ 'id', 'nome', 'descripcion', 'avatar', 'dateCreated', 'lastUpdated', 'username' ]
	}

	// datos para a aplicación
	String nome
	String descripcion
	String avatar = 'assets/sen-avatar.png'
	Conta conta
	Date dateCreated 
	Date lastUpdated

	// spring security
	String username // email ou admin
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static transients = [ 'springSecurityService' ]

	Set<Role> getAuthorities() {
		PersoaRole.findAllByPersoa(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

	static hasMany = [ anuncios: Anuncio ]
	static constraints = {
		nome blank: false, unique: true
		descripcion nullable: true
		conta nullable: true
		username blank: false, unique: true
		password blank: false
	}
	static mapping = {
		descripcion type: 'text'
		avatar type: 'text'
		password column: '`password`'
	}
}
