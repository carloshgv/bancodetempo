package bancodetempo

class Mensaxe {
	String texto
	Date dateCreated 
	Date lastUpdated 

	static belongsTo = [ conversa: Conversa ]
    static constraints = {
    	texto blank: false, nullable: false
    }
}
