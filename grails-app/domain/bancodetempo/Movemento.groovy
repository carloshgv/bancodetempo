package bancodetempo

class Movemento {
	int horas
    int minutos
	String concepto
	Date dateCreated 
	Date lastUpdated 

	static belongsTo = [ levantamento: Conta, ingreso: Conta ]
    static constraints = {
    	levantamento validator: { val, obj -> val != obj.ingreso }
    	ingreso validator: { val, obj -> val != obj.levantamento }
    	horas min: 0, validator: { val, obj -> if(val == 0) obj.minutos != 0 }
        minutos min: 0, max: 59
    	concepto blank: false
    }

    def beforeInsert() {
        if(minutos > 59) {
            horas += Math.floor(minutos / 60)
            minutos = minutos % 60
        }
    }

    def beforeUpdate() {
        if(isDirty('minutos')) {
            if(minutos > 59) {
                horas += Math.floor(minutos / 60)
                minutos = minutos % 60
            }
        }
    }    
}
