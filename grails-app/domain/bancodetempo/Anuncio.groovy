package bancodetempo

class Anuncio {
	String texto
	Date dateCreated = new Date()
	Date lastUpdated 

	static belongsTo = [ anunciante: Persoa ]
	static constraints = {
		texto blank: false, nullable: true
	}
	static mapping = {
		texto type: 'text'
		anunciante fetch: 'join'
	}
	static searchable = {
		anunciante reference: true
	}

	static {
		grails.converters.JSON.registerObjectMarshaller(Anuncio) { anuncio -> 
			[
				id: anuncio.id,
				texto: anuncio.texto,
				dateCreated: anuncio.dateCreated,
				anunciante: [
					id: anuncio.anunciante.id,
					nome: anuncio.anunciante.nome,
					avatar: anuncio.anunciante.avatar
				]
			]
		}
	}
}