package bancodetempo

import org.apache.commons.lang.builder.HashCodeBuilder

class PersoaRole implements Serializable {
	private static final long serialVersionUID = 1

	Persoa persoa
	Role role
	Date dateCreated 
	Date lastUpdated 

	boolean equals(other) {
		if (!(other instanceof PersoaRole)) {
			return false
		}

		other.persoa?.id == persoa?.id &&
		other.role?.id == role?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (persoa) builder.append(persoa.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static PersoaRole get(long persoaId, long roleId) {
		PersoaRole.where {
			persoa == Persoa.load(persoaId) &&
			role == Role.load(roleId)
		}.get()
	}

	static boolean exists(long persoaId, long roleId) {
		PersoaRole.where {
			persoa == Persoa.load(persoaId) &&
			role == Role.load(roleId)
		}.count() > 0
	}

	static PersoaRole create(Persoa persoa, Role role, boolean flush = false) {
		def instance = new PersoaRole(persoa: persoa, role: role)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(Persoa u, Role r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = PersoaRole.where {
			persoa == Persoa.load(u.id) &&
			role == Role.load(r.id)
		}.deleteAll()

		if (flush) { PersoaRole.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(Persoa u, boolean flush = false) {
		if (u == null) return

		PersoaRole.where {
			persoa == Persoa.load(u.id)
		}.deleteAll()

		if (flush) { PersoaRole.withSession { it.flush() } }
	}

	static void removeAll(Role r, boolean flush = false) {
		if (r == null) return

		PersoaRole.where {
			role == Role.load(r.id)
		}.deleteAll()

		if (flush) { PersoaRole.withSession { it.flush() } }
	}

	static constraints = {
		role validator: { Role r, PersoaRole ur ->
			if (ur.persoa == null) return
			boolean existing = false
			PersoaRole.withNewSession {
				existing = PersoaRole.exists(ur.persoa.id, r.id)
			}
			if (existing) {
				return 'persoaRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['role', 'persoa']
		version false
	}
}
