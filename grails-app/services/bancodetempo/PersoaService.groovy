package bancodetempo

import grails.transaction.Transactional
import org.hibernate.criterion.CriteriaSpecification
import org.apache.commons.lang.RandomStringUtils
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import org.springframework.security.authentication.BadCredentialsException

@Transactional
class PersoaService {
	def elasticSearchHelper, springSecurityService
	
	def perfil(id) {
		Persoa.withCriteria(uniqueResult: true) {
			resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
			ilike 'username', "%@%"
			eq 'id', id
			projections {
				property 'id', 'id'
				property 'nome', 'nome'
				property 'avatar', 'avatar'
				property 'descripcion', 'descripcion'
				property 'username', 'email'
				conta {
					property 'id', 'conta'
				}
			}
		}
	}

	def actualizar(Persoa persoa) {
		// gardar a nova foto de perfil
		// tokenize the data
		def parts = persoa.avatar.tokenize(",")
		def imageString = parts[1]

		// create a buffered image
		BufferedImage image = null
		byte[] imageByte

		Base64.Decoder decoder = Base64.getDecoder()
		imageByte = decoder.decode(imageString)
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte)
		image = ImageIO.read(bis)
		bis.close()

		// write the image to a file
		// o path debería ser externo en producción
		def filename = RandomStringUtils.random(16, true, true)
		def path = "grails-app/assets/images/profile-pictures/${filename}.png"
		File outputfile = new File(path)
		ImageIO.write(image, "png", outputfile)
		persoa.avatar = "assets/profile-pictures/${filename}.png"
		persoa.save()
	}

	def mudarContrasinal(Persoa persoa, antigo, novo) {
		if(!springSecurityService.passwordEncoder.isPasswordValid(persoa.password, antigo, null))
			throw new BadCredentialsException('o contrasinal antigo non é válido')
		persoa.password = novo
		persoa.save()
	}

	def conta(id) {
		Persoa.withCriteria(uniqueResult: true) {
			resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
			eq 'id', id
			ilike 'username', "%@%"
			join 'conta'
			projections {
				conta {
					property 'id', 'conta'
					property 'horas', 'horas'
					property 'minutos', 'minutos'
				}
			}
		}
	}

	def perfilEConta(id) {
		Persoa.withCriteria(uniqueResult: true) {
			resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
			eq 'id', id
			ilike 'username', "%@%"
			join 'conta'
			projections {
				property 'id', 'id'
				property 'nome', 'nome'
				property 'avatar', 'avatar'
				property 'descripcion', 'descripcion'
				property 'username', 'email'
				conta {
					property 'id', 'conta'
					property 'horas', 'horas'
					property 'minutos', 'minutos'
				}
			}
		}
	}

	def buscarNomesComo(busqueda) {
		busqueda = busqueda ?: ''
		Persoa.withCriteria {
			resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
			ilike 'nome', "${busqueda}%"
			ilike 'username', "%@%"
			join 'conta'
			projections {
				property 'id', 'id'
				property 'nome', 'nome'
				conta {
					property 'id', 'conta'
				}
			}
		}
	}

	def buscar(busqueda, desde) {
		def result = elasticSearchHelper.withElasticSearch { client ->
			client.prepareSearch('bancodetempo')
				.setTypes('persoa')
				.setQuery(org.elasticsearch.index.query.QueryBuilders.queryString(busqueda))
				.setFrom(desde ?: 0).setSize(10)
				.addHighlightedField('descripcion', 0, 0)
				.execute().actionGet()
		}

		[ 
			total: result.hits.totalHits,
			searchResults: result.hits.collect {
				[
					nome: it.source.nome,
					avatar: it.source.avatar,
					descripcion: it.highlightFields?.descripcion?.fragments?.join('&ellip;') ?: it.source.descripcion
				]
			}
		]
	}
}
