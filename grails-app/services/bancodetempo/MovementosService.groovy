package bancodetempo

import grails.transaction.Transactional

@Transactional
class MovementosService {
	def novo(Movemento movemento) throws IllegalArgumentException {
		def de = movemento.levantamento?.persoa
		def para = movemento.ingreso?.persoa

		if(!de)
			throw new IllegalArgumentException('conta de saída inexistente')
		if(!para)
			throw new IllegalArgumentException('conta de destino inexistente')
		if(de == para)
			throw new IllegalArgumentException('non se pode transferir á mesma persoa')
		if(movemento.horas < 0)
			throw new IllegalArgumentException('as horas teñen que ser maiores ou iguais que 0')
		if(movemento.minutos < 0 || movemento.minutos > 59)
			throw new IllegalArgumentException('os minutos teñen que estar entre 0 e 59')
		if(de.conta.horas < movemento.horas)
			throw new IllegalArgumentException('non tes horas suficientes')
		if(de.conta.horas == 0 && de.conta.minutos < movemento.minutos)
			throw new IllegalArgumentException('non tes horas suficientes')

		movemento.levantamento.horas -= movemento.horas
		movemento.levantamento.minutos -= movemento.minutos
		if(movemento.levantamento.minutos < 0) {
			movemento.levantamento.horas--;
			movemento.levantamento.minutos += 60;
		}

		movemento.ingreso.horas += movemento.horas
		movemento.ingreso.minutos += movemento.minutos
		if(movemento.ingreso.minutos > 59) {
			movemento.ingreso.horas++;
			movemento.ingreso.minutos -= 60;
		}

		movemento.levantamento.save()
		movemento.ingreso.save()
		movemento.save()
		movemento
	}

	def de(Persoa persoa, max = 0) {
		def persoaId = persoa.id
		Movemento.executeQuery(
			'select de.nome, de.id, para.nome, para.id, m.horas, m.minutos, m.concepto, m.dateCreated ' +
			'from Movemento as m ' +
			'inner join m.levantamento as l ' +
			'inner join m.ingreso as i ' +
			'inner join l.persoa as de ' +
			'inner join i.persoa as para ' +
			'where de.id = :persoaId or para.id = :persoaId ' +
			'order by m.dateCreated desc', [ persoaId: persoa.id ], max? [ max: max ] : [:]
		)
		.collect {
			[
				de: [ nome: it[0], id: it[1] ],
				para: [ nome: it[2], id: it[3] ],
				horas: it[4],
				minutos: it[5],
				concepto: it[6],
				dateCreated: it[7]
			]
		}
	}
}
