package bancodetempo

import java.text.SimpleDateFormat
import grails.transaction.Transactional

import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.search.SearchType
import org.elasticsearch.index.query.FilterBuilders.*
import org.elasticsearch.index.query.QueryBuilders.*

@Transactional
class AnunciosService {
	def elasticSearchHelper

	def getUltimos(offset = 0) {
		Anuncio.list(max: 10, sort: 'dateCreated', order: 'desc', offset: offset)
	}

	def getUltimosDesde(desde) {
		Anuncio.findAllByDateCreatedGreaterThan(desde, [ sort: 'dateCreated', order: 'asc' ])
	}
	
	def getEditadosDesde(desde, idsAConsultar) {
		Anuncio.withCriteria {
			join 'anunciante'
			'in' 'id', idsAConsultar
			gt 'lastUpdated', desde
			order 'dateCreated', 'asc'
			maxResults 10
		}
	}

	def ultimoDe(persoa) {
		Anuncio.findByAnunciante(persoa, [ max: 1, sort: 'dateCreated', order: 'desc' ]);
	}

	def de(persoa, offset = 0) {
		Anuncio.findAllByAnunciante(persoa, [ max: 10, offset: offset, sort: 'dateCreated', order: 'desc', join: 'anunciante' ]);
	}

	def total() {
		Anuncio.count()
	}

	def buscar(busqueda, desde) {
		def result = elasticSearchHelper.withElasticSearch { client ->
			client.prepareSearch('bancodetempo')
				.setTypes('anuncio')
				.setQuery(org.elasticsearch.index.query.QueryBuilders.queryString(busqueda))
				.setFrom(desde ?: 0).setSize(10)
				.addHighlightedField('texto', 0, 0)
				.execute().actionGet()
		}

		[ 
			total: result.hits.totalHits,
			searchResults: result.hits.collect {
				[
					anunciante: Persoa.get(it.source.anunciante.id),
					dateCreated: it.source.dateCreated,
					lastUpdated: it.source.lastUpdated,
					texto: it.highlightFields.texto.fragments.join('&ellip;')
				]
			}
		]
	}
}
