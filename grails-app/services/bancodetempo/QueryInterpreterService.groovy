package bancodetempo

class QueryInterpreterService {
	def prepare(String input) {
		def sanitized = input.replaceAll(~/~|"|#|\+|-|\.|<|>|\*|:|\?|\[|\]|\{|\}|\^|\\|\!/, ' ')
		def terms = sanitized.split(' ')*.trim()*.plus('~1')
		def phrase = "\"$sanitized\"~1^2"
		def query
		if(terms.size > 1)
			query = [ phrase, terms.join(' OR ') ].join(' OR ')
		else
			query = terms[0]
		query
	}
}
