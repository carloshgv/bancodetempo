navigation = {
	admin {
		admin(titleText: 'Estado', data: [ icon: 'mdi-action-account-balance' ])
		persoas(data: [ icon: 'mdi-social-people' ])
		movementos(data: [ icon: 'mdi-action-swap-horiz' ])
		anuncios(data: [ icon: 'mdi-notification-sms' ])
		cheques(data: [ icon: 'mdi-image-style' ])
		sair(url: '/logout', data: [ icon: 'mdi-action-highlight-remove' ])
	}
}