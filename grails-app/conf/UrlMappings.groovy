class UrlMappings {
	static excludes = [
		'/chatroom*',
		'/j_*',
		'/logout'
	]

	static mappings = {
		"/login/$action"(controller: 'login')

		// admin
		'/admin'(controller: 'admin', action: 'index' )
		'/admin/persoas'(controller: 'persoas', action: 'index')
		'/admin/anuncios'(controller: 'anuncios', action: 'index')
		'/admin/movementos'(controller: 'movementos', action: 'index')

		// users
		"/$controller/$action?/$id?"()
		"/persoas/$persoa/password"(controller: 'persoas', action: 'password')
		"/$controller/item/$id?"(parseRequest: true) {
			action = [ GET: 'show', POST: 'save', DELETE: 'delete', PUT: 'update' ]
		}

		// app
		"/auth/$action"(controller: 'auth')
        '/**?'(view: '/index')
        '500'(view: '/error')
	}
}
