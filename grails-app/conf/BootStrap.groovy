import bancodetempo.*
import java.util.Random

class BootStrap {
	def random = new Random();

	def init = { servletContext ->
		def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
		def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
		def admin = new Persoa(nome: 'Admin', username: 'admin', password: 'admin')
		admin.save(flush: true, failOnError: true)

		PersoaRole.create admin, adminRole, true		

		def nomes = [
			'Prince Aungst',
			'Dorthey Henninger',
			'Lakisha Salmons',
			'Vernon Padillo',
			'Jess Sheppard',
			'Eneida Fullwood',
			'Neville Cruz',
			'Elin Pracht',
			'Josefina Ragan',
			'Adelina Re',
			'Dorinda Kingsland',
			'Vincenzo Mcnab',
			'Ira Peace',
			'Helaine Every',
			'Chere Horsey',
			'Milagros Petters',
			'Hui Bulluck',
			'Tricia Pabst',
			'Katherine Prum',
			'Milton Haglund',
			'Jacquiline Speight',
			'Theodora Thigpen',
			'Youlanda Lynd',
			'Kerstin Stocks',
			'Karie Laroche'
		]
		def senhas = [
			'prince',
			'dorthey',
			'lakisha',
			'vernon',
			'jess',
			'eneida',
			'neville',
			'elin',
			'josefina',
			'adelina',
			'dorinda',
			'vincenzo',
			'ira',
			'helaine',
			'chere',
			'milagros',
			'hui',
			'tricia',
			'katherine',
			'milton',
			'jacquiline',
			'theodora',
			'youlanda',
			'kerstin',
			'karie'
		]
		def anuncios = [
			'Convenience store ablative human sunglasses concrete savant skyscraper plastic geodesic Kowloon euro-pop bridge lights rebar. Augmented reality RAF papier-mache camera jeans grenade dissident military-grade city construct render-farm. Systemic 8-bit concrete face forwards dissident uplink faded geodesic grenade shrine singularity neon. Soul-delay systemic savant tower woman artisanal footage girl. Network voodoo god saturation point industrial grade 8-bit camera gang tanto.',
			'Skyscraper network cartel ablative military-grade chrome San Francisco geodesic computer order-flow numinous bridge meta- apophenia market carbon. Drone media knife fetishism monofilament tanto camera. A.I. nodality beef noodles post- rain shoes marketing boat ablative skyscraper concrete face forwards boy garage. Receding San Francisco -space range-rover papier-mache shrine tanto. Kanji paranoid denim Chiba numinous katana dissident city papier-mache Legba nano- monofilament smart- futurity. Pistol Tokyo cardboard papier-mache fetishism assault artisanal. Dead A.I. sentient industrial grade weathered otaku cyber- motion marketing vinyl claymore mine euro-pop. Tattoo shoes refrigerator math- bicycle singularity plastic denim dome convenience store augmented reality physical wonton soup. Hacker euro-pop garage stimulate Kowloon camera lights motion.',
			'Sprawl long-chain hydrocarbons tank-traps neural RAF render-farm fluidity boy sensory assassin. Camera decay Kowloon vehicle apophenia silent rebar convenience store cardboard engine BASE jump fetishism Chiba. Concrete order-flow denim narrative smart- sub-orbital advert disposable tiger-team rifle shoes uplink engine. Otaku free-market Legba advert tiger-team sensory range-rover computer soul-delay warehouse shrine sunglasses render-farm Chiba. Silent soul-delay Kowloon rifle euro-pop film woman. Assassin garage advert meta- woman marketing cyber- film construct girl.',
			'Corrupted market fluidity motion ablative cardboard shrine paranoid drone sensory San Francisco. Otaku crypto- smart- footage Kowloon cartel tank-traps long-chain hydrocarbons receding hacker RAF dissident monofilament neon faded. Realism otaku fluidity fetishism city spook tank-traps boy dead footage shanty town table systema skyscraper math- boat. Chiba dead hotdog otaku bomb euro-pop office face forwards rain rifle drugs shanty town. Into motion advert alcohol receding corrupted DIY San Francisco bicycle engine wristwatch sprawl youtube sub-orbital. Plastic camera sub-orbital bridge nano- disposable saturation point towards market. 8-bit decay rain render-farm franchise San Francisco neon towards faded Kowloon. Drone meta- Tokyo digital assassin warehouse human franchise engine fluidity boat bicycle modem narrative claymore mine. Face forwards -ware pen systema long-chain hydrocarbons nodal point rain artisanal apophenia sentient soul-delay digital.',
			'Concrete dolphin narrative math- convenience store j-pop boat urban neon savant shanty town alcohol nodality Legba face forwards tanto order-flow. Drugs saturation point alcohol decay marketing grenade voodoo god geodesic neon uplink nodality San Francisco faded. Futurity free-market soul-delay youtube systema pre- Tokyo knife pistol. Apophenia hotdog 8-bit tiger-team vehicle cartel beef noodles. Grenade into drone industrial grade sensory concrete realism sentient katana euro-pop savant meta- bicycle cardboard knife camera. Corporation drone advert apophenia cardboard into tiger-team bridge tanto bomb lights BASE jump ablative urban kanji. Nodality tanto -ware boy silent beef noodles computer bomb. Bomb into footage bridge monofilament geodesic military-grade DIY marketing BASE jump tattoo denim. BASE jump alcohol carbon dead vehicle refrigerator shrine boy crypto- saturation point shanty town euro-pop sunglasses network urban tank-traps.',
			'Otaku chrome franchise sensory disposable narrative hacker ablative -ware. J-pop skyscraper denim shoes city bomb franchise film pre- Chiba cartel market. Lights franchise corrupted footage digital dead bicycle ablative voodoo god claymore mine tattoo 8-bit drone youtube. Shoes drugs carbon market systemic DIY rebar Kowloon human nano- semiotics bicycle rifle katana. Claymore mine systema smart- monofilament systemic alcohol vehicle 3D-printed order-flow narrative shanty town gang.',
			'Gang digital fetishism plastic motion boy tube vinyl dissident. Refrigerator modem katana tower jeans meta- fetishism skyscraper film dissident construct warehouse denim tattoo. DIY range-rover refrigerator RAF euro-pop Shibuya carbon nano- wonton soup digital skyscraper post- dead bicycle media construct jeans. Legba monofilament DIY bridge spook industrial grade wristwatch j-pop. Motion rain receding urban convenience store claymore mine tanto sprawl render-farm soul-delay. Denim futurity uplink jeans saturation point rebar corrupted carbon papier-mache modem youtube claymore mine.',
			'Office franchise Kowloon crypto- human assassin shoes uplink engine silent footage pre-. Camera engine futurity BASE jump sprawl otaku math- towards sunglasses neural car dissident industrial grade media. Drugs j-pop sunglasses spook courier motion man plastic cartel. Media pen man bridge free-market assault rebar into corrupted woman car face forwards modem marketing warehouse bicycle sub-orbital. Corrupted camera vinyl wristwatch modem augmented reality assault drugs pre- rebar knife. Camera beef noodles spook computer cartel sign network Tokyo tiger-team stimulate. Hotdog render-farm office rifle Kowloon savant voodoo god saturation point tower cartel decay range-rover sensory camera.',
			'Artisanal papier-mache faded convenience store dolphin pen stimulate rebar skyscraper footage math- media otaku gang. Network sign motion meta- girl nodality concrete. Long-chain hydrocarbons boy systema car Chiba uplink shrine vehicle RAF carbon table spook free-market knife modem. Assault range-rover lights 8-bit warehouse augmented reality sprawl boat. Shoes disposable -space math- sensory vehicle silent gang San Francisco Tokyo. Concrete soul-delay urban carbon singularity geodesic wonton soup tattoo stimulate narrative pistol range-rover neural bridge augmented reality fluidity tanto. Wonton soup tanto drugs RAF nodality wristwatch long-chain hydrocarbons digital cyber- camera rain claymore mine katana Kowloon. Corrupted city carbon pre- digital dome assassin motion girl beef noodles render-farm. Disposable pre- dead silent rifle table skyscraper bridge carbon nodality wristwatch.',
			'DIY fluidity film corporation vinyl weathered wonton soup monofilament towards alcohol. Augmented reality lights tank-traps paranoid decay beef noodles DIY bomb rebar San Francisco dead tanto advert dolphin. Post- savant advert corporation futurity rifle singularity tanto office DIY pre- jeans table bicycle drone narrative augmented reality. Nodality free-market vehicle tattoo tanto wonton soup nodal point hotdog katana chrome shrine skyscraper digital.',
		]
		def descripcions = [
			'Sei algo de programación en Java, C, C#, python. Tamén me gusta a pintura e o debuxo',
			'Estou en primeiro de camiños, controlo bastante de matemáticas (cálculo, álxebra...). No meu tempo libre dou clases de twae-kwondo',
			'Acabei a carreira de informática hai un par de anos e agora traballo facendo aplicacións web. Tamén deseño videoxogos e cando podo colaboro dando charlas con organizacións ecoloxistas',
		]

		[nomes, senhas].transpose().each { nome, senha ->
			println "$nome : $senha"

			def p = new Persoa(
				nome: "$nome",
				username: "$senha@banco",
				password: "$senha",
				avatar: "assets/test-profiles/profile (${random.nextInt(20)+1}).png",
				descripcion: "${descripcions[random.nextInt(3)]}",
			)
			p.save(flush: true, failOnError: true)
			PersoaRole.create p, userRole, true		

			def c = new Conta(persoa: p).save(flush: true, failOnError: true)

			3.times {
				def a = new Anuncio(
					texto: anuncios[random.nextInt(anuncios.size())],
					anunciante: p
				)
				a.save(flush: true, failOnError: true)
			}
		}
	}
	
	def destroy = {
	}
}
