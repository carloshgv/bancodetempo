package bancodetempo

import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.web.JSONBuilder

class AnunciosController {
	static allowedMethods = [ save: 'POST', delete: 'DELETE', update: 'PUT' ]
	def anunciosService, springSecurityService, messageSource, queryInterpreterService

	def index() {
		def anuncios = anunciosService.getUltimos(params.int('offset') ?: 0)
		withFormat {
			json { 
				render(anuncios as JSON)
			}
			html anuncios: anuncios
		}
	}

	def novos() {
		def anuncios = anunciosService.getUltimosDesde(new Date(params.long('desde') + 1000))
		render(anuncios as JSON)
	}

	def editados() {
		def idsAConsultar = params.ids.split(',')*.toLong()
		def anuncios = anunciosService.getEditadosDesde(new Date(params.long('desde')), idsAConsultar)
		render(anuncios as JSON)
	}

	def buscar() {
		def resultados = anunciosService.buscar(queryInterpreterService.prepare(params.q), params.int('desde'));
		render(resultados as JSON)
	}

	@Secured('ROLE_USER')
	def save(Anuncio anuncio) {
		if(anuncio.anunciante.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ])
		} else {
			anuncio.texto = anuncio.texto.replaceAll("<(.|\n)*?>", '')
			if(anuncio.save(flush: true))
				render(anuncio as JSON)
			else {
				response.status = 500
				render([ error: messageSource.getMessage(anuncio.errors.allErrors[0], new Locale('pt')) ] as JSON)
			}
		}
	}

	@Secured('ROLE_USER')
	def delete(Anuncio anuncio) {
		if(anuncio.anunciante.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ])
		} else {
			anuncio.delete(flush: true)
			render([ msg: 'ok' ] as JSON)
		}
	}

	@Secured('ROLE_USER')
	def update(Anuncio anuncio) {
		if(anuncio.anunciante.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ])
		} else {
			anuncio.texto = anuncio.texto.replaceAll("<(.|\n)*?>", '')
			anuncio.save(flush: true)
			render([ msg: 'ok' ] as JSON)
		}
	}

	def total(Persoa persoa) {
		if(persoa)
			render([ total: Anuncio.countByAnunciante(persoa) ] as JSON)
		else
			render([ total: Anuncio.count() ] as JSON)
	}

	def ultimoDe(Persoa persoa) {
		render(anunciosService.ultimoDe(persoa) as JSON)
	}

	def de(Persoa persoa) {
		def offset = params.int('offset') ?: 0
		render(anunciosService.de(persoa, offset) as JSON)
	}
}
