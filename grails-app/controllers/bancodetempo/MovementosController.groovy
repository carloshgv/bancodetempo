package bancodetempo

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class MovementosController {
	def movementosService, springSecurityService

	@Secured('ROLE_USER')
	def save(Movemento movemento) {
		if(movemento.levantamento.persoa.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ] as JSON)
		} else {
			try {
				render(movementosService.novo(movemento) as JSON)
			} catch(IllegalArgumentException ex) {
				response.status = 500
				render([ error: ex.message ] as JSON)
			}
		}
	}

	@Secured('ROLE_USER')
	def ultimasDe(Persoa persoa) {
		if(persoa.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ] as JSON)
		} else {
			render(movementosService.de(persoa, 5) as JSON)
		}
	}

	@Secured('ROLE_USER')
	def de(Persoa persoa) {
		if(persoa.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ] as JSON)
		} else {
			render(movementosService.de(persoa) as JSON)
		}
	}
}
