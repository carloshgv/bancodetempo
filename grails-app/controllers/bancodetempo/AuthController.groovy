package bancodetempo

import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import org.apache.commons.lang.math.RandomUtils

class AuthController {
	static allowedMethods = [ login: 'POST' ]
	def persoaService, springSecurityService

	@Secured('ROLE_USER')
	def perfil() {
		def persoa = springSecurityService.isLoggedIn()? springSecurityService.loadCurrentUser() : null;
		if(persoa) {
			render(persoaService.perfilEConta(persoa.id) as JSON)
		}
		else {
			response.status = 401
			render([ info: 'sesión non iniciada' ] as JSON)
		}
	}
}
