package bancodetempo

import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.web.JSONBuilder
import org.springframework.security.authentication.BadCredentialsException

class PersoasController {
	static allowedMethods = [ nomes: 'GET', buscar: 'GET', password: 'PUT' ]
	def persoaService, springSecurityService, messageSource, queryInterpreterService

	@Secured(['ROLE_ADMIN'])
	def index() {
		def q = params.q? "%$params.q%" : '%'
		params.max = params.max ?: 10

		def resultado = Persoa.findAllByNomeIlike("$q", params)
		def count = Persoa.countByNomeIlike("$q")
		[ persoas: resultado, totalPersoas: count ]
	}

	@Secured(['ROLE_ADMIN'])
	def nova() {
		def p = new Persoa(params)
		if(p.save()) {
			redirect([ controller: 'persoas', action: 'index' ])
		} else {
			flash.message = 'Operación non completada'
			redirect([ controller: 'persoas', action: 'index' ])
		}
	}

	@Secured(['ROLE_ADMIN'])
	def desactivar(Persoa persoa) {
		persoa.enabled = false
		persoa.save(flush: true)
		redirect([ controller: 'persoas', action: 'index' ])
	}

	@Secured(['ROLE_ADMIN'])
	def activar(Persoa persoa) {
		persoa.enabled = true
		persoa.save(flush: true)
		redirect([ controller: 'persoas', action: 'index' ])
	}

	@Secured(['ROLE_USER'])
	def update(Persoa persoa) {
		if(persoa.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ])
		} else {
			if(persoaService.actualizar(persoa))
				render([ msg: 'ok' ] as JSON)
			else {
				response.status = 500
				render([ error: messageSource.getMessage(persoa.errors.allErrors[0], new Locale('pt')) ] as JSON)
			}
		}
	}

	@Secured(['ROLE_USER'])
	def password() {
		def persoa = Persoa.get(params.persoa)
		def contrasinal = request.JSON
		if(persoa.id != springSecurityService.loadCurrentUser().id) {
			response.status = 403
			render([ error: 'non autorizada' ] as JSON)
		} else {
			try {
				persoaService.mudarContrasinal(persoa, contrasinal.antigo, contrasinal.novo)
				render([ msg: 'ok' ] as JSON)
			} catch(BadCredentialsException e) {
				response.status = 401
				render([ error: e.message ] as JSON)
			}
		}
	}

	def buscar() {
		render(persoaService.buscar(queryInterpreterService.prepare(params.q), params.int('desde')) as JSON)
	}

	def nomes() {
		render(persoaService.buscarNomesComo(params.como) as JSON)
	}

	def show(Persoa persoa) {
		def perfil = persoaService.perfil(persoa.id)
		if(perfil)
			render(perfil as JSON)
		else {
			response.status = 404
			render([ error: 'o perfil non existe' ] as JSON)
		}
	}

	@Secured(['ROLE_USER'])
	def conta(Persoa persoa) {
		render(persoaService.conta(persoa.id) as JSON)
	}
}
