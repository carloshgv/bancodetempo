package bancodetempo

class MaterializeCSSTagLib {
	static defaultEncodeAs = [taglib:'raw']
	static namespace = 'm'

	def paginate = { attrs, body ->
		def total = attrs.total ?: 10
		def controller = attrs.controller ?: controllerName
		def action = attrs.action ?: actionName
		def maxSteps = attrs.int('maxsteps') ?: 10
		def keepParams = attrs.params ?: params

		def offset = params.int('offset') ?: 0
		def max = params.int('max') ?: attrs.int('max') ?: 10
		def totalSteps = (int)(total / max) - (total % max == 0? 1I : 0I)
		def currentStep = (int)(offset / max)
		def firstVisibleStep, lastVisibleStep

		def halfMaxSteps = (int)(maxSteps/2)
		def even = 1 - maxSteps % 2
		if(currentStep < halfMaxSteps)
			(firstVisibleStep, lastVisibleStep) = [ 0, Math.min(totalSteps, maxSteps - 1) ]
		else if(currentStep >= totalSteps - halfMaxSteps)
			(firstVisibleStep, lastVisibleStep) = [ Math.max(totalSteps - maxSteps + 1, 0), totalSteps ]
		else
			(firstVisibleStep, lastVisibleStep) = [ currentStep - halfMaxSteps + even, currentStep + halfMaxSteps ]

		if(totalSteps > 0) {
			out << """<ul class="pagination">"""

			// prev arrow
			def linkParams = keepParams + [ offset: Math.max(offset - max, 0) ]
			def link = createLink([ controller: controller, action: action, params: linkParams ])
			if(currentStep <= 0)
				out << """<li class="disabled"><i class="mdi-navigation-chevron-left"></i></li>"""
			else
				out << """<li><a href="$link"><i class="mdi-navigation-chevron-left"></i></a></li>"""

			(firstVisibleStep..lastVisibleStep).each {
				linkParams = keepParams + [ offset: it * max ]
				link = createLink([ controller: controller, action: action, params: linkParams ])
				if(currentStep == it)
					out << """<li class="active">${it+1}</li>"""
				else
					out << """<li><a href="$link">${it+1}<a></li>"""
			}

			// next arrow
			linkParams = keepParams + [ offset: Math.min(total - total % max, offset + max) ]
			link = createLink([ controller: controller, action: action, params: linkParams ])
			if(currentStep >= totalSteps)
				out << """<li class="disabled"><i class="mdi-navigation-chevron-right"></i></li>"""
			else
				out << """<li><a href="$link"><i class="mdi-navigation-chevron-right"></i></a></li>"""
			
			out << """</ul>"""
		}
	}
}
