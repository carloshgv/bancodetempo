<html>
	<head>
		<title>Persoas</title>
		<meta name="layout" content="admin/main"/>
	</head>
	<body>
		<g:form name="busca" url="[ controller: 'persoas' ]" method="GET">
			<div class="row" class="valign-container">
				<g:set var="formClass" value="s12"/>
				<g:if test="${params.q}">
					<g:set var="formClass" value="s11"/>
				</g:if>
				<div class="input-field col ${formClass}">
					<i class="mdi-action-search prefix"></i>
					<g:textField type="search" name="q" value="${params.q}"/>
					<label for="q">buscar</label>
				</div>
				<input type="hidden" name="max" value="10"/>
				<g:if test="${params.q}">
					<div class="col s1">
						<g:link controller="persoas">
							<i class="mdi-content-clear small valign"></i>
						</g:link>
					</div>
				</g:if>
			</div>
		</g:form>
		<g:if test="${!persoas}">
			<p class="error">Non se atoparon persoas</p>
		</g:if>
		<g:if test="${flash.message}">
			<p class="error">${flash.message}</p>
		</g:if>
		<div class="row">
			<div class="center-align">
				<m:paginate params="[ q: params?.q ]" 
					maxsteps="11" 
					controller="persoas"
					total="${totalPersoas}"/>
			</div>
		</div>
		<ul class="collapsible collection">
			<li>
				<div class="collapsible-header collection-item avatar waves-effect waves-light teal lighten-4 black-text" id="novaPersoaDialog">
					<asset:image src="icons/ic_add_circle_24px.svg" class="circle"/>
					<span class="title">Engadir persoa</span>
				</div>
				<div class="collapsible-body grey lighten-4 black-text">
					<g:form name="nova"						
					        controller="persoas"
					        action="nova"
					        method="POST"
					        class="content">
						<div class="row">
							<div class="input-field col s6">
								<g:textField tabindex="1" type="text" name="nome" class="validate" required="true"/>
								<label for="nome">nome</label>
							</div>
							<div class="input-field col s6">
								<g:passwordField tabindex="2" name="password" 
									class="validate" required="true" onchange="validarPasswords()"/>
								<label for="password">contrasinal</label>
							</div>
							<div class="input-field col s6 offset-s6">
								<g:passwordField tabindex="3" name="repetir-password" 
									class="validate" required="true" onchange="validarPasswords()"/>
								<label for="repetir-password">repetir</label>
							</div>
							<div class="input-field col s12">
								<g:field tabindex="4" type="email" name="username" class="validate" required="true"/>
								<label for="username">e-mail</label>
							</div>
							<div class="input-field col s12">
								<g:textField tabindex="5" name="descripcion"/>
								<label for="descripcion">descripción</label>
							</div>
						</div>
						<div class="row last">
							<div class="right-align col s12">
								<button tabindex="7" type="reset" class="btn-flat red-text text-lighten-3 waves-effect no-margin"
									    onclick="$('#novaPersoaDialog').click()">Cancelar</button>
								<button tabindex="6" type="submit" class="btn teal waves-effect no-margin">Enviar</button>
							</div>
						</div>
					</g:form>
				</div>
			</li>
			<g:each in="${persoas}" var="persoa">
				<li>
					<div class="collapsible-header collection-item avatar waves-effect waves-purple black-text">
						<img src="<g:createLink url='/${persoa.avatar}'/>" class="circle"/>
						<span class="title">${persoa.nome} - <a href="mailto:${persoa.username}">${persoa.username}</a></span>
						<p>
							${persoa.descripcion}<br/>
							<g:if test="${!persoa.enabled}"><span class="red-text">conta desactivada</span></g:if>
						</p>
					</div>
					<div class="collapsible-body grey lighten-4">
						<g:if test="${persoa.enabled}">
							<g:link controller="persoas" action="desactivar" params="[ id: persoa.id ]" class="btn red waves-effect">
								Desactivar
							</g:link>
						</g:if>
						<g:else>
							<g:link controller="persoas" action="activar" params="[ id: persoa.id ]" class="btn waves-effect">
								Activar
							</g:link>
						</g:else>
					</div>
				</li>
			</g:each>
		</ul>
		<script type="text/javascript">
			function validarPasswords() {
				console.log($('#password').val() + '?==' + $('#repetir-password').val());
				console.log($('#password').val() === $('#repetir-password').val());

				if($('#password').val() !== $('#repetir-password').val()) {
					$('#repetir-password')[0].setCustomValidity('Os contrasinais non coinciden');
				} else {
					$('#repetir-password')[0].setCustomValidity('');
				}
			}
		</script>
	</body>
</html>
