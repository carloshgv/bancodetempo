<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Banco de tempo</title>
		<base href="/"/>
		<link id="apiRoot" href=""/>
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
		<asset:stylesheet href="application.css"/>
		<asset:javascript src="application.js"/>
	</head>
	<body ng-app="bancoDeTempo" ng-controller="appController" layout="row">
		<div ng-if="loading" style="width: 100vw; height: 100vh; position: fixed; top: 0; left: 0; background-color: #DDD; z-index: 1000"><span us-spinner="{radius:30, width:8, length: 16}"></span></div>
		<md-sidenav md-swipe-left="pecharMenu()" class="site-sidenav md-sidenav-left md-whiteframe-z2 ng-cloak"
				md-component-id="left"
				md-is-locked-open="$media('gt-sm') && tab != 2">
			<md-toolbar>
				<h1 class="md-toolbar-tools">Banco de Tempo</h1>
			</md-toolbar>
			<div ng-if="!session" ng-include=" 'login-form.htm' "></div>
			<div style="overflow-y: auto">
				<div ng-if="session" ng-include=" 'user-info-bar.htm' " ng-controller="userBarController"></div>
			</div>
		</md-sidenav>

		<div layout="column" role="main" flex class="ng-cloak">
			<md-toolbar hide-gt-sm class="md-toolbar-tools md-toolbar-tools-bottom">
				<button class="menu-icon" ng-click="mostrarMenu()" aria-label="menu">
					<md-icon style="color:white" md-svg-src="assets/icons/ic_menu_48px.svg"></md-icon>
				</button>
				<span hide-gt-sm layout="column" layout-align="center" class="search-box-container">
					<input type="text" ng-model="q" class="search-box toolbar-form" ng-keypress="buscar($event, busqueda)"/>
				</span>
			</md-toolbar>
			<div layout="row" class="main-bar" flex>
				<button class="menu-icon tabs" hide-sm ng-if="tab == 2" ng-click="mostrarMenu()" aria-label="menu">
					<md-icon style="color:white" md-svg-src="assets/icons/ic_menu_48px.svg"></md-icon>
				</button>
				<md-tabs class="md-primary" md-selected="tab" flex>
					<md-tab ui-sref="index">
						<md-tab-label>
							<div layout="row" layout-align="center center">
								<md-icon style="color:white" md-svg-src="assets/icons/ic_announcement_48px.svg"></md-icon>
								<span>Anuncios</span>
							</div>
						</md-tab-label>
					</md-tab>
					<md-tab ui-sref="grupos">
						<md-tab-label>
							<div layout="row" layout-align="center center">
								<md-icon style="color:white" md-svg-src="assets/icons/ic_group_48px.svg"></md-icon>
								<span>Grupos</span>
							</div>
						</md-tab-label>
					</md-tab>
					<md-tab ng-show="!loading && (session || tab == 2)" ui-sref="perfil.anuncios({ id: session.id })">
						<md-tab-label>
							<div layout="row" layout-align="center center">
								<md-icon style="color:white" md-svg-src="assets/icons/ic_home_48px.svg"></md-icon>
								<span>Perfil</span>
							</div>
						</md-tab-label>
					</md-tab>
					<md-tab ng-show="session" ui-sref="mensaxes">
						<md-tab-label>
							<div layout="row" layout-align="center center">
								<md-icon style="color:white" md-svg-src="assets/icons/ic_mail_48px.svg"></md-icon>
								<span>Mensaxes</span>
							</div>
						</md-tab-label>
					</md-tab>
				</md-tabs>
				<span hide-sm layout="column" layout-align="center" class="search-box-container">
					<input type="text" ng-model="busqueda" class="search-box toolbar-form" ng-keypress="buscar($event, busqueda)"/>
				</span>
			</div>
			<div class="main-content" infinite-scroll ui-view/>
		</div>
	</body>
</html>
