<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title><g:layoutTitle default="Administración do banco de tempo"/></title>
		<g:layoutHead/>
		<asset:javascript src="jquery/dist/jquery.js"/>
		<asset:javascript src="materialize/dist/js/materialize.js"/>
		<asset:stylesheet href="admin/all.css"/>
	</head>
	<body>
		<header>
			<nav class="top-nav fixed pink lighten-2 white-text">
				<div class="container">
					<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full">
						<i class="mdi-navigation-menu"></i>
					</a>
					<div class="nav-wrapper">
						<a class="page-title">${params.section ? params.section.capitalize() : "Estado"}</a>
					</div>
				</div>
			</nav>
			<ul class="side-nav fixed" id="nav-mobile">
				<div class="pink lighten-2 white-text">
					<li class="page-title">Banco</li>
				</div>
				<nav:menu scope="admin" custom="true">
					<li <g:if test="${active || "admin/$params.section" == item.id}">class="active"</g:if>>
						<p:callTag tag="g:link" attrs="${linkArgs}">
							<i class="${item.data.icon} tiny"></i>
							<nav:title item="${item}"/>
						</p:callTag>
					</li>
				</nav:menu>
			</ul>
		</header>
		<main>
			<div class="container">
				<g:layoutBody/>
			</div>
		</main>
		<script type="text/javascript">
			$(".button-collapse").sideNav();
		</script>
	</body>
</html>

<%--
<li class="no-padding">
	<ul class="collapsible collapsible-accordion">
		<li class="bold">
			<a class="collapsible-header waves-effect">Exemplo</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="#">asfasdf</a></li>
					<li><a href="#">asfasdf</a></li>
					<li><a href="#">asfasdf</a></li>
					<li><a href="#">asfasdf</a></li>
					<li><a href="#">asfasdf</a></li>
				</ul>
			</div>
		</li>
	</ul>
</li>
--%>
