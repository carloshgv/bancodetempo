<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<meta name="layout" content="main"/>
	<title>Entrar na administración</title>
	<asset:javascript src="jquery/dist/jquery.js"/>
	<asset:javascript src="materialize/dist/js/materialize.js"/>
	<asset:stylesheet href="admin/all.css"/>
	<style type="text/css">
		body {
			width: 100%;
			padding-top: 50px;
		}

		.container {
			width: 400px;
			margin: 0 auto !important;
		}

		@media only screen and (max-width: 600px) {
		.container {
			width: 90%;
		}
	</style>
</head>

<body>
<div class="container">
	<div class="card">
		<div class="card-title green lighten-1">Autenticación</div>
		<form class="no-margin" action="${postUrl}" method="POST" id="loginForm" autocomplete="off">
			<div class="card-content">
				<g:if test="${flash.message}">
					<div class="login_message">${flash.message}</div>
				</g:if>

					<div class="row">
						<div class="input-field col s12">
							<label for="username">nome</label>
							<g:textField type="text" class="text_" name="j_username" id="username"/>
						</div>
						<div class="input-field col s12">
							<label for="password">contrasinal</label>
							<g:passwordField type="password" class="text_" name="j_password" id="password"/>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<input type="checkbox" class="chk" name="${rememberMeParameter}" id="remember_me" <g:if test="${hasCookie}">checked="checked"</g:if>/>
							<label for="remember_me">lembrar</label>
						</div>
					</div>
			</div>
			<div class="card-action right-align">
				<button type="submit" id="submit" class="green-text btn-flat no-margin waves-effect waves-light">Entrar</button>
			</div>
		</form>
	</div>
</div>
</body>
</html>
