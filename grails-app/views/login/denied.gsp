<head>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name='layout' content='main' />
<title><g:message code="springSecurity.denied.title" /></title>
</head>

<body>
<div class='body'>
	<div class='errors'><g:message code="springSecurity.denied.message"/></div>
	<a href="/logout">Sair</a>
</div>
</body>
