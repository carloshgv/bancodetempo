<html>
	<head>
		<meta name="layout" content="admin/main"/>
	</head>
	<body>
	<div class="row with-margin">
		<g:each in="${(1..7)}" var="i">
			<div class="col s12 m6 xl4">
				<div class="card ">
					<div class="card-title pink lighten-2">Casilla ${i}</div>
					<div class="card-content">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</div>
			</div>
		</g:each>
	</body>
</html>