app.service('persoasService', function($http, $q, apiRoot) {
	return {
		buscarNomesComo: function(busqueda) {
			return $http.get(apiRoot + '/persoas/nomes', { params: { como: busqueda || '' } })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});
		},

		perfil: function(persoaId) {
			return $http.get(apiRoot + '/persoas/item/' + persoaId)
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});			
		},

		actualizar: function(persoa) {
			return $http.put(apiRoot + '/persoas/item/' + persoa.id, persoa)
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});			
		},

		mudarContrasinal: function(persoa, antigo, novo) {
			return $http.put(apiRoot + '/persoas/' + persoa.id + '/password', { antigo: antigo, novo: novo })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});			
		},

		conta: function(persoaId) {
			return $http.get(apiRoot + '/persoas/conta/' + persoaId)
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});			
		},

		buscar: function(q, desde) {
			if(!q)
				return $q.reject({ error: 'búsqueda vacía' });
			var params = { q: q };
			if(desde) params.desde = desde;
			return $http.get(apiRoot + '/persoas/buscar', { params: params })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		}		
	}
});