app.service('transferenciasService', function($mdDialog, $http, $q, apiRoot) {
	return {
		pedirDatos: function($event, para) {
			return $mdDialog.show({
				templateUrl: 'dialogo-transferencia.htm',
				targetEvent: $event,
				controller: 'dialogoTransferenciaController',
				locals: { para: para }
			});
		},

		transferir: function(de, a, horas, minutos, concepto) {
			return $http.post(apiRoot + '/movementos/item/', {
				levantamento: de,
				ingreso: a,
				horas: horas,
				minutos: minutos,
				concepto: concepto
			})
			.then(function(response) {
				if(typeof response.data === 'object')
					return response.data;
				else
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});
		},

		ultimasDe: function(idPersoa) {
			return $http.get(apiRoot + '/movementos/ultimasDe/' + idPersoa)
			.then(function(response) {
				if(typeof response.data === 'object')
					return response.data;
				else
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});
		},

		de: function(idPersoa) {
			return $http.get(apiRoot + '/movementos/de/' + idPersoa)
			.then(function(response) {
				if(typeof response.data === 'object')
					return response.data;
				else
					return $q.reject(response.data);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});
		}
	};
});