app.service('anunciosService', function($http, $q, $rootScope, apiRoot) {
	return {
		getAnuncios: function(primeiro) {
			primeiro = primeiro || 0;

			return $http.post(apiRoot + '/anuncios', { params: { format: 'json', offset: primeiro } })
			.then(function(res) {
				if(typeof res.data === 'object') {
					return res.data;
				}
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			});
		},

		publicar: function(anuncio) {
			return $http.post(apiRoot + '/anuncios/item/', anuncio)
			.then(function(response) {
				if(typeof response.data === 'object') {
					response.data.anunciante.nome = $rootScope.session.nome
					response.data.anunciante.avatar = $rootScope.session.avatar
					return response.data
				} else
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		eliminar: function(anuncio) {
			return $http.delete(apiRoot + '/anuncios/item/' + anuncio.id)
			.then(function(response) {
				if(typeof response.data !== 'object')
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		actualizar: function(anuncio) {
			return $http.put(apiRoot + '/anuncios/item/' + anuncio.id, anuncio)
			.then(function(response) {
				if(typeof response.data !== 'object')
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		total: function(id) {
			return $http.get(apiRoot + '/anuncios/total/' + (id || ''))
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data.total;
				} else 
					return 0;
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		novosDesde: function(data) {
			return $http.get(apiRoot + '/anuncios/novos', { params: { desde: data } })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		actualizadosDesde: function(data, ids) {
			return $http.get(apiRoot + '/anuncios/editados', { params: { desde: data, ids: ids } })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		ultimoDe: function(idPersoa) {
			return $http.get(apiRoot + '/anuncios/ultimoDe/' + idPersoa)
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		de: function(idPersoa, primeiro) {
			var params = primeiro? { offset: primeiro } : {};
			return $http.get(apiRoot + '/anuncios/de/' + idPersoa, { params: params })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		},

		buscar: function(q, desde) {
			if(!q)
				return $q.reject({ error: 'búsqueda vacía' });
			var params = { q: q };
			if(desde) params.desde = desde;
			return $http.get(apiRoot + '/anuncios/buscar', { params: params })
			.then(function(response) {
				if(typeof response.data === 'object') {
					return response.data;
				} else 
					return $q.reject(response);
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else {
					return $q.reject({ error: 'erro interno do servidor', raw: err.data });
				}
			});
		}
	};
});
