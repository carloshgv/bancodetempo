app.service('dateFormatter', function() {
	return {
		elapsed: function (timestamp) {
			var from = new Date(timestamp).getTime();
			var now = new Date().getTime();
			var elapsed = (new Date(now - from)) / 1000; // difference in seconds
			if(elapsed < 60)
				return 'Hai menos dun minuto';
			else if(elapsed >= 60 && elapsed < 120)
				return 'Hai un minuto';
			else if(elapsed >= 120 && elapsed < 3600)
				return 'Hai ' + Math.floor(elapsed / 60) + ' minutos';
			else if(elapsed >= 3600 && elapsed < 7200)
				return 'Hai unha hora';
			else if(elapsed >= 7200 && elapsed < 86400)
				return 'Hai ' + Math.floor(elapsed / 3600) + ' horas';
			else if(elapsed >= 86400 && elapsed < 172800)
				return 'Hai un dia';
			else if(elapsed >= 172800 && elapsed < 604800)
				return 'Hai ' + Math.floor(elapsed / 86400) + ' días';
			else if(elapsed >= 604800 && elapsed < 1209600)
				return 'Hai unha semana';
			else if(elapsed >= 1209600 && elapsed < 4233600)
				return 'Hai ' + Math.floor(elapsed / 604800) + ' semanas';
			else if(elapsed >= 4233600 && elapsed < 8467200)
				return 'Hai un mes';
			else if(elapsed >= 8467200 && elapsed < 50803200)
				return 'Hai ' + Math.floor(elapsed / 4233600) + ' meses';
			else if(elapsed >= 50803200 && elapsed < 101606400)
				return 'Hai un ano';
			else if(elapsed >= 101606400)
				return 'Hai ' + Math.floor(elapsed / 50803200) + ' anos';
		}
	};
});
