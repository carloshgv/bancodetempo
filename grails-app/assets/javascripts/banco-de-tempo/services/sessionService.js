app.service('sessionService', function($http, $q, apiRoot) {
	var obterPerfil = function() {
		return $http({
			method: 'GET',
			url: apiRoot + '/auth/perfil',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
			}
		})
		.then(function(response) {
			return response.data;
		})
		.catch(function(err) {
			return $q.reject(err.data);
		});
	};

	return {
		obterPerfil: obterPerfil,

		login: function(email, password, manter) {
			return $http({
				method: 'POST',
				url: apiRoot + '/j_spring_security_check?ajax=true',
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: {
					j_username: email,
					j_password: password,
					_spring_security_remember_me: manter
				}
			})
			.then(function(response) {
				if(response.data.error)
					return $q.reject(response);
				return response.data.username;
			})
			.then(function(email) {
				return obterPerfil();
			})
			.catch(function(err) {
				if(typeof err.data === 'object')
					return $q.reject(err.data.error);
				else
					return $q.reject('Erro interno do servidor');
			});
		}
	};
});