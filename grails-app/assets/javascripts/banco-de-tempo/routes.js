app.config(function($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider, $provide) {
	$locationProvider.html5Mode(true);
	$urlRouterProvider
		.when('/logout', function($window) {
			$window.location.reload();
			return true;
		})
		.otherwise('/');

	// por defecto, angular codifica os espacios nas urls como %20 antes de
	// poñelas na barra de enderezos do navegador. De poñelas encárgase o servizo
	// $browser (que é unha api privada, así que isto non se debería facer), que recibe a url 
	// xa codificada. Por tanto, trocamos os %20 que xerou o $location antes de mudar
	// a barra de enderezos, e volvémolos deixar como estaban cando alguén lle pida a url
	// ao $browser, para non rompermos o restdo do sistema
	$provide.decorator('$browser', function($delegate) {
		superUrl = $delegate.url;
		$delegate.url = function(url, replace, state) {
			if(url)
				return superUrl(url.replace(/\%20/g, '+'), replace, state);
			else
				return superUrl(url, replace, state).replace(/\+/g, '%20');
		}
		return $delegate;
	});

	$stateProvider
		.state('index', {
			url: '/',
			template: '<md-content class="md-padding">' +
					  '    <novo-anuncio ng-if="session"></novo-anuncio>' +
					  '    <feed items="anuncios" total="totalAnuncios" cargar-mais="cargarMaisAnuncios()" editabel="editabel(anuncio)"></feed>' +
					  '</md-content>',
			controller: 'anunciosController',
			tabIndex: 0
		})
		.state('grupos', {
			url: '/grupos',
			tabIndex: 1,
			templateUrl: 'grupos.htm'
		})
		.state('perfil', {
			abstract: true,
			url: '/perfil/:id',
			templateUrl: 'perfil.htm',
			controller: 'perfilController'
		})
			.state('perfil.anuncios', {
				url: '',
				tabIndex: 2,
				template: '<md-content class="md-padding">' +
						  '    <novo-anuncio ng-if="(perfil && session && perfil.id == session.id)"></novo-anuncio>' +
						  '    <feed items="anuncios" total="totalAnuncios" cargar-mais="cargarMaisAnuncios()" editabel="anuncioEditabel(anuncio)"></feed>' +
						  '</md-content>',
			})		
			.state('perfil.movementos', {
				url: '/movementos',
				tabIndex: 2,
				templateUrl: 'transferencias.htm'
			})
		.state('mensaxes', {
			url: '/mensaxes',
			tabIndex: 3,
			templateUrl: 'mensaxes.htm',
			controller: 'chatController'
		})
		.state('buscar', {
			abstract: true,
			url:'/buscar?q',
			templateUrl: 'buscar.htm',
			controller: 'buscarController',
		})
			.state('buscar.anuncios', {
				url:'/anuncios',
				template: '<feed items="atopados" total="total" cargar-mais="buscarMais()" key="{{ ::key }}"></feed>',
				controller: 'buscarAnunciosController',
				tabIndex: 4,
				busquedaTab: 0
			})
			.state('buscar.persoas', {
				url:'/persoas',
				templateUrl: 'lista-persoas.htm',
				controller: 'buscarPersoasController',
				tabIndex: 4,
				busquedaTab: 1
			});
});
