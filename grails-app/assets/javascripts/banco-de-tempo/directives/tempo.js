app.directive('tempo', function() {
	return {
		restrict: 'E',
		scope: {
			horas: '=',
			minutos: '=',
			class: '@'
		},
		template: '<span class="{{ class }}">' +
		          '	{{ horas }}<md-icon style="position: relative; top: -1px" md-svg-src="assets/icons/ic_timer_48px.svg" class="{{ class }} grey"></md-icon>' + 
		          '	{{ minutos }}' +
		          '</span>'
	}
});