app.directive('editorAvatar', function() {
	return {
		restrict: 'E',
		scope: {
			orixinal: '=',
			recortada: '=',
			class: '@'
		},
		template: '<div class="avatar grande" style="cursor: move">' +
				  '    <canvas></canvas>' +
				  '</div>' +
				  '<span layout="row" layout-align="end" style="margin-top: -36px">' +
				  '    <md-button class="md-primary md-raised btn-file" aria-label="Cargar foto">' +
				  '        <md-icon style="color:white" md-svg-src="assets/icons/ic_camera_alt_48px.svg"></md-icon>' +
				  '        <input type="file" id="fileInput"/>' +
				  '    </md-button>' +
				  '</span>',
		link: function($scope, element, attrs) {
			var canvas = element.find('canvas')[0];
			var container = element.find('div');
			var width = container.width();
			var height = container.height();
			var x = 0, y = 0;
			$(canvas).attr('width', width);
			$(canvas).attr('height', height);
			var context = canvas.getContext('2d');
			var image = new Image();
			var zoom = 1;

			var update = function() {
				// rectangulo (para gardar)
				context.clearRect(0, 0, width, height);
				context.save();
				context.translate(width/2, height/2);
				context.scale(zoom, zoom);
				context.drawImage(image, x, y, image.width * scale, image.height * scale);
				$scope.$apply(function($scope) {
					if(!dragging) {
						$scope.recortada = canvas.toDataURL();
					}
				});
				context.restore();

				// círculo (para ver como queda)
				context.clearRect(0, 0, width, height);
				context.save();
				context.beginPath();
				context.arc(width/2, height/2, width/2, 0, 2*Math.PI);
				context.clip();
				context.translate(width/2, height/2);
				context.scale(zoom, zoom);
				context.drawImage(image, x, y, image.width * scale, image.height * scale);
				context.restore();
				context.beginPath();
				context.arc(width/2, height/2, width/2 + 1, 0, 2*Math.PI);
				context.lineWidth = 3;
				context.strokeStyle = "#DDD";
				context.stroke();
				context.restore();
			};

			var init = function() {
				if(image.width > image.height)
					scale = width / image.width;
				else
					scale = height / image.height;
				x = -image.width / 2 * scale;
				y = -image.height / 2 * scale;
				zoom = 1;
				update();
			}

			image.onload = init;
			image.src = $scope.orixinal;

			var handleFileSelect = function(evt) {
				var file = evt.currentTarget.files[0];
				var reader = new FileReader();
				reader.onload = function (evt) {
					image.src = evt.target.result;
				};
				reader.readAsDataURL(file);
			};
			$('#fileInput').on('change', handleFileSelect);

			// eventos
			var dragX, dragY, dragging = false;
			element.on('mousewheel DOMMouseScroll', function(event) {
				event.preventDefault();
				if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
					zoom = Math.max(zoom * 0.90, 0.01);
				}
				else {
					zoom = zoom / 0.90;
				}
				update();
			});

			element.on('mousedown', function(event) {
				event.preventDefault();
				var mouseX = event.clientX - canvas.offsetLeft;
				var mouseY = event.clientY - canvas.offsetTop;
				dragX = mouseX;
				dragY = mouseY;
				dragging = true;
			});

			element.on('mousemove', function(event) {
				event.preventDefault();
				if(dragging) {
					var mouseX = event.clientX - canvas.offsetLeft;
					var mouseY = event.clientY - canvas.offsetTop;

					x += (mouseX - dragX)/zoom;
					y += (mouseY - dragY)/zoom;
					dragX = mouseX;
					dragY = mouseY;
					update();
				}
			});

			element.on('mouseup', function(event) {
				if(dragging)
					dragging = false;
				update();
			});
		}
	};
});	
