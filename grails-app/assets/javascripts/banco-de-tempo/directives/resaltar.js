app.directive('resaltar', function($timeout) {
	return {
		restrict: 'A',
		scope: {
			resaltar: '@'
		},
		link: function($scope, element, attrs) {
			$timeout(function() {
				var regexs = $scope.resaltar.split(' ').map(function(palabra) { 
					return new RegExp('\\b('+palabra+')\\b', "gi"); 
				});
				regexs.forEach(function(regex) {
					element[0].innerHTML = element[0].innerHTML.replace(regex, '<span class="resaltado">$1</span>');
				});
			});
		}
	}
});
