app.directive('elapsedTime', function(dateFormatter, $interval) {
	return {
		restrict: 'EA',
		template: '<span>' + 
		          '  <md-tooltip md-direction="right">{{ prettyFrom }}</md-tooltip>' + 
		          '  <span class="timestamp">{{ time }}</span>' +
		          '</span>',
		scope: {
			from: '='
		},
		link: function($scope, element, attrs, controller, transclude) {
			function update() {
				$scope.time = dateFormatter.elapsed($scope.from);
				$scope.prettyFrom = new Date($scope.from).toLocaleString();
			}
			update();
			$interval(update, 120000); // actualizar cada dous minutos
			$scope.$watch('from', update);
		}
	}
});