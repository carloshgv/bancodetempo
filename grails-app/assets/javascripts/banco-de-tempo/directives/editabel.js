app.directive('contenteditable', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function($scope, element, attrs, ctrl) {
			var colarTextoPlano = function(ev) {
				ev.preventDefault();
				var textoPlano = ev.clipboardData.getData('text/plain');
				document.execCommand('inserttext', false, textoPlano);
			}
			element[0].addEventListener('paste', colarTextoPlano);
			// view -> model
			element.bind('blur', function() {
				$scope.$apply(function() {
					ctrl.$setViewValue(element.text());
				});
			});

			// model -> view
			ctrl.$render = function() {
				element.text(ctrl.$viewValue);
			};

			// load init value from DOM
			ctrl.$setViewValue(element.text());
		}
	}
});
