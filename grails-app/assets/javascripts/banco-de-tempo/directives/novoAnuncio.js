app.directive('novoAnuncio', function($compile, $rootScope, $sce, $mdDialog, eventos, nl2br, anunciosService) {
	return {
		restrict: 'EA',
		templateUrl: 'novo-anuncio.htm',
		scope: false,
		link: function($scope, element, attrs) {
			$scope.publicar = function(anuncio) {
				anuncio.anunciante = $scope.session.id;
				anuncio.texto = String(anuncio.texto).replace(/<[^>]+>/gm, '');		
				anunciosService.publicar(anuncio)
				.then(function(response) {
					$scope.anuncios.unshift(response);
					$rootScope.$broadcast(eventos.perfilCambiado);
					anuncio.texto = '';
				})
				.catch(function(error) {
					$mdToast.showSimple({
						content: 'Erro ao publicar o anuncio: ' + error,
						hideDelay: 3000,
						position: 'top right'
					});
				});
			};
		}
	}
});
