app.directive('transferencia', function() {
	return {
		restrict: 'E',
		scope: {
			de: '=',
			para: '=',
			horas: '=',
			minutos: '=',
			concepto: '=',
			completa: '=',
			fecha: '=',
			class: '@'
		},
		templateUrl: 'transferencia.htm'
	}
})