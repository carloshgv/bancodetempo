app.directive('feed', function() {
	return {
		restrict: 'EA',
		templateUrl: 'feed.htm',
		scope: {
			items: '=',
			total: '=',
			key: '@',
			cargarMais: '&',
			editabel: '&'
		},
		controller: function() {}
	}
});