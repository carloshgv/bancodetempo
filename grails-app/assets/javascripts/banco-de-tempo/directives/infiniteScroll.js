app.directive('infiniteScroll', function(eventos, $window) {
	return {
		restrict: 'A',
		link: function($scope, element, attrs) {
			element.bind('scroll', function() {
				if(element[0].offsetHeight + element[0].scrollTop >= element[0].scrollHeight) {
					$scope.$broadcast(eventos.finScroll);
				}
			});
		}
	};
});

app.directive('onScrollEnd', function(eventos) {
	return {
		restrict: 'A',
		link: function($scope, element, attrs) {
			$scope.$on(eventos.finScroll, function() {
				$scope.$eval(attrs.onScrollEnd);
			});
		}
	}
});
