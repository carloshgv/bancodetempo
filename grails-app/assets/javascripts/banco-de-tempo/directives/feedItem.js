app.directive('feedItem', function($compile, $rootScope, $sce, $mdDialog, eventos, nl2br, anunciosService) {
	return {
		restrict: 'EA',
		require: '^feed',
		templateUrl: 'feedItem.htm',
		scope: true,
		link: function($scope, element, attrs) {
			var buttonsHtml = '<div ng-show="editando" class="edit-buttons" layout="row" layout-align="end">' + 
			                  '    <md-button ng-click="cancelar()">Cancelar</md-button>' + 
			                  '    <md-button ng-click="aceptar()" class="md-primary md-raised">Aceptar</md-button>' + 
			                  '</div>';
			var buttons = $($compile(buttonsHtml)($scope));
			var texto = $(element).find('p.texto');
			var contidoOrixinal = $scope.item.texto;
			$scope.textoHtml = $sce.trustAsHtml(nl2br($scope.item.texto));
			buttons.appendTo(texto.parent());

			// métodos para o item
			$scope.editar = function() {
				if(texto.attr('contenteditable') == 'false') {
					$rootScope.$broadcast(eventos.editarAnuncio, $scope.item.id);
					texto.attr('contenteditable', 'true');
					$scope.editando = true;
				}
			};

			$scope.aceptar = function() {
				if(texto.attr('contenteditable') == 'true') {
					texto.attr('contenteditable', 'false');
					contidoOrixinal = texto.html().replace(/<br\/?>/gm, '\n');
					contidoOrixinal = contidoOrixinal.replace(/<[^>]+>/gm, '');
					$scope.editando = false;
					$scope.item.texto = contidoOrixinal;
					anunciosService.actualizar($scope.item)
					.then(function() {
						$rootScope.$broadcast(eventos.perfilCambiado);
					})
					.catch(function(err) {
						$mdToast.showSimple({
							content: 'Erro ao editar o anuncio: ' + error,
							hideDelay: 3000,
							position: 'top right'
						});
					});
				}
			};

			$scope.cancelar = function() {
				texto.html(nl2br(contidoOrixinal));
				texto.attr('contenteditable', 'false');
				$scope.editando = false;
			};

			$scope.eliminar = function($event) {
				var confirmar = $mdDialog.confirm()
					.title('Seguro que queres borrar este anuncio?')
					.content($scope.item.texto)
					.ok('Borrar')
					.targetEvent($event)
					.cancel('Mellor non');

				$mdDialog.show(confirmar)
				.then(function() {
					anunciosService.eliminar($scope.item)
					.then(function(response) {
						$rootScope.$broadcast(eventos.perfilCambiado);
						var index = $scope.items.indexOf($scope.item);
						$scope.items.splice(index, 1);
					})
					.catch(function(error) {
						$mdToast.showSimple({
							content: 'Erro ao eliminar o anuncio: ' + error,
							hideDelay: 3000,
							position: 'top right'
						});
					});
				});
			}

			$scope.$on(eventos.finSesion, function() {
				if(texto.attr('contenteditable') == 'true')
					$scope.cancelar();
			});

			$scope.$on(eventos.editarAnuncio, function(id) {
				if(id != $scope.item.id && texto.attr('contenteditable') == 'true')
					$scope.cancelar();
			});
		}
	}
});