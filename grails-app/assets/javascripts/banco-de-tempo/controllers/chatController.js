app.controller('chatController', function() {
	var log = $("#log");
	var message = $("#message");
	var sendMessageButton = $("#send-message-button");

	// Create the WebSocket URL link. This URI maps to the URI you specified
	// in the @ServerEndpoint annotation in ChatroomEndpoint.groovy.
	webSocketUrl = "ws://192.168.0.13:8080/chatroom";

	// Connect to the WebSocket.
	socket = new WebSocket(webSocketUrl);

	socket.onopen = function () {
		log.append("<p>Connected to server. Enter your username.</p>");
	};

	socket.onmessage = function (message) {
		log.append("<p>" + message.data + "</p>");
	};

	socket.onclose = function () {
		log.append("<p>Connection to server was lost.</p>");
	};

	sendMessageButton.on('click', function() {
		var text = message.val();
		if ($.trim(text) !== '') {
			// Send the message and clear the text.
			socket.send(text);

			message.val("");
			message.focus();
			return;
		}
		message.focus();
	});
});
