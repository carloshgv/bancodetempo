app.controller('appController', function($scope, $rootScope, $mdSidenav, $state, $interval, sessionService, eventos) {
	$scope.tab = 0;
	
	// eventos
	$scope.$on(eventos.iniciarSesion, function(event, info) {
		$rootScope.session = info;
	});

	$scope.$on(eventos.perfilCambiado, function() {
		sessionService.obterPerfil().then(
		function(data) {
			$rootScope.session = data;
		});
	});
	$interval(function() { $rootScope.$broadcast(eventos.perfilCambiado); }, 60000);

	$scope.tab = $state.current.tabIndex;
	$scope.$on('$stateChangeStart', function(ev, to) {
		if(to.name.indexOf('perfil') == 0)
			$scope.pecharMenu();
		$scope.tab = to.tabIndex;
	});

	$scope.mostrarMenu = function() {
		$mdSidenav('left').open();
	};

	$scope.pecharMenu = function() {
		$mdSidenav('left').close();
	};

	$scope.buscar = function($event, busqueda) {
		if($event.which == 13 && busqueda)
			$state.go('buscar.anuncios', { q: busqueda });
	}
});