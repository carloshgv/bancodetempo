app.controller('perfilController', function($scope, $stateParams, $state, $rootScope, $timeout, $window, $animate,
											$mdToast, $mdDialog, eventos, persoasService, anunciosService, transferenciasService) {
	$scope.perfil = {};

	// cargar os anuncios sempre que se carga o perfil vai moi lento e queda feo
	var cargarPerfil = function(id) {
		anunciosService.total(id)
		.then(function(total) {
			$scope.totalAnuncios = total;
		});

		transferenciasService.de(id)
		.then(function(movementos) {
			$scope.movementos = movementos;
		});

		persoasService.conta(id)
		.then(function(conta) {
			$scope.perfil.horas = conta.horas;
			$scope.perfil.minutos = conta.minutos;
		});

		return persoasService.perfil(id)
		.then(function(perfil) {
			for(var attr in perfil)
				$scope.perfil[attr] = perfil[attr];

			return $scope.perfil;
		})
		.catch(function(err) {
			$state.go('index');
		});
	}

	$scope.tab = 2;
	cargarPerfil($stateParams.id)
	.then(function(perfil) {
		anunciosService.de(perfil.id)
		.then(function(anuncios) {
			anuncios.forEach(function(a) { a.anunciante = perfil; });
			$scope.anuncios = anuncios;
		});
	});

	$rootScope.$on('$stateChangeStart', function(ev, to, paramsTo, from, paramsFrom) {
		if(to.name.indexOf('perfil') == 0 && 
			(from.name.indexOf('perfil') == -1 || paramsTo.id != paramsFrom.id)) {
			cargarPerfil(paramsTo.id)
			.then(function(perfil) {
				anunciosService.de(perfil.id)
				.then(function(anuncios) {
					anuncios.forEach(function(a) { a.anunciante = perfil; });
					$scope.anuncios = anuncios;
				});
			});
		}
	});

	$rootScope.$on(eventos.perfilCambiado, function() {
		if($state.includes('perfil') && !$scope.editandoPerfil) {
			cargarPerfil($stateParams.id);
		}
	});
	$rootScope.$on(eventos.iniciarSesion, function() {
		if($state.includes('perfil') && !$scope.editandoPerfil) {
			cargarPerfil($stateParams.id);
		}
	});

	$scope.logout = function() {
		$window.location.href = '/logout';
	};

	$scope.anuncioEditabel = function(anuncio) {
		return $rootScope.session && anuncio.anunciante.id == $rootScope.session.id;
	}

	$scope.facerTransferencia = function($event, para) {
		transferenciasService.pedirDatos($event, para)
		.then(function(para) {
			$rootScope.$broadcast(eventos.perfilCambiado);
		});
	};

	$scope.mudarContrasinal = function($event) {
		return $mdDialog.show({
			templateUrl: 'mudar-contrasinal.htm',
			targetEvent: $event,
			controller: 'mudarContrasinalController'
		});
	};

	$scope.verMovementos = function() {
		$state.go('perfil.movementos', { id: $scope.perfil.id });
	};

	$scope.verAnuncios = function() {
		$state.go('perfil.anuncios', { id: $scope.perfil.id });
	};

	$scope.cargarMaisAnuncios = function() {
		if($scope.anuncios.length == $scope.totalAnuncios)
			return;
		$scope.cargandoMais = true;
		$animate.enabled(false);
		anunciosService.de($stateParams.id, $scope.anuncios.length)
		.then(function(anuncios) {
			anuncios.forEach(function(a) { a.anunciante = $scope.perfil; });
			$scope.anuncios = $scope.anuncios.concat(anuncios);
		})
		.catch(function(err) {
			console.log('erro cargando máis anuncios: ' + err.error);
		})
		.finally(function() {
			$scope.cargandoMais = false;
			$timeout(function() { $animate.enabled(true); }, 500);
		});
	};

	var orixinais;
	$scope.editarPerfil = function() {
		$scope.editandoPerfil = true;
		var campos = $('.perfil [contenteditable]');
		campos.attr('contenteditable', true);

		orixinais = { 
			nome: $scope.perfil.nome, 
			descripcion: $scope.perfil.descripcion,
			avatar: $scope.perfil.avatar
		};
	};

	$scope.gardarPerfil = function() {
		$scope.editandoPerfil = false;
		$scope.perfil.avatar = $scope.perfil.novoAvatar;
		$scope.perfil.novoAvatar = undefined;
		var campos = $('.perfil [contenteditable]');
		campos.attr('contenteditable', false);
		persoasService.actualizar({
			id: $scope.perfil.id,
			nome: $scope.perfil.nome,
			descripcion: $scope.perfil.descripcion,
			avatar: $scope.perfil.avatar
		})
		.then(function() {
			$rootScope.$emit(eventos.perfilCambiado);
			$rootScope.session.avatar = $scope.perfil.avatar;
			$rootScope.session.nome = $scope.perfil.nome;
		})
		.catch(function(err) {
			$mdToast.showSimple({
				content: 'Erro ao editar o perfil: ' + err.error,
				hideDelay: 3000,
				position: 'top right'
			});
		});
	};

	$scope.cancelarEdicion = function() {
		$scope.editandoPerfil = false;
		$scope.perfil.novoAvatar = undefined;
		var campos = $('.perfil [contenteditable]');
		campos.attr('contenteditable', false);
		for(var i in orixinais)
			$scope.perfil[i] = orixinais[i];
	};
});
