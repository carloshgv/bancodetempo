app.controller('buscarAnunciosController', function($stateParams, $scope, $animate, anunciosService) {
	$scope.$parent.busquedaAnuncios.then(function() {
		$scope.atopados = $scope.$parent.anuncios;
		$scope.total = $scope.$parent.anunciosTotal;
	});

	$scope.buscarMais = function() {
		if($scope.atopados.length == $scope.total)
			return;
		$scope.cargandoMais = true;
		$animate.enabled(false);
		anunciosService.buscar($stateParams.q, $scope.atopados.length)
		.then(function(atopados) {
			$scope.atopados = $scope.atopados.concat(atopados.searchResults);
		})
		.catch(function(err) {
			console.log('erro cargando máis anuncios: ' + err.error);
		})
		.finally(function() {
			$scope.cargandoMais = false;
			$timeout(function() { $animate.enabled(true); }, 500);
		});
	};
});
