app.controller('mudarContrasinalController', function($mdDialog, $rootScope, $scope, $q,
                                                          persoasService) {
	$scope.aceptar = function() {
		persoasService.mudarContrasinal($rootScope.session, $scope.antigo, $scope.novo)
		.then(function() {
			$mdDialog.hide();
		})
		.catch(function(err) {
			$scope.sinalForm.mensaxeErro = err.error;	
		});
	};

	$scope.cancelar = function() {
		$mdDialog.cancel();
	};
});