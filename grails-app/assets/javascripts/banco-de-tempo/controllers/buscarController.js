app.controller('buscarController', function($scope, $stateParams, $state, $sce, $q, anunciosService, persoasService) {
	$scope.q = $stateParams.q;
	$scope.key = $stateParams.q;
	$scope.busquedaTab = $state.current.busquedaTab;
	$scope.buscando = true;

	$scope.busquedaPersoas = persoasService.buscar($stateParams.q)
	.then(function(atopadas) {
		$scope.persoas = atopadas.searchResults.map(function(persoa) {
			return $.extend(persoa, { descripcion: $sce.trustAsHtml(persoa.descripcion) });
		});
		$scope.persoasTotal = atopadas.total;
	});

	$scope.busquedaAnuncios = anunciosService.buscar($stateParams.q)
	.then(function(anuncios) {
		$scope.anuncios = anuncios.searchResults;
		$scope.anunciosTotal = anuncios.total;
	});

	$q.all([ $scope.busquedaPersoas, $scope.busquedaAnuncios ])
	.then(function() {
		$scope.buscando = false;
	});

	$scope.buscar = function($event, busqueda) {
		if($event.which == 13) {
			if(busqueda.length > 0)
				$state.go('.', { q: busqueda });
			else
				$scope.q = $stateParams.q;
		}
	};
});
