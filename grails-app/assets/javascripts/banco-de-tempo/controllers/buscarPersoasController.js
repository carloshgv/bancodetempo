app.controller('buscarPersoasController', function($stateParams, $scope, $animate, $sce, persoasService) {
	$scope.$parent.busquedaPersoas.then(function() {
		$scope.persoas = $scope.$parent.persoas;
		$scope.total = $scope.$parent.persoasTotal;
	});

	$scope.buscarMais = function() {
		if($scope.persoas.length == $scope.total)
			return;
		$scope.cargandoMais = true;
		$animate.enabled(false);
		persoasService.buscar($stateParams.q, $scope.persoas.length)
		.then(function(atopadas) {
			$scope.persoas = $scope.persoas.concat(atopadas.searchResults.map(function(atopada) {
				return $.extend(atopada, { descripcion: $sce.trustAsHtml(atopada.descripcion) });
			}));
		})
		.catch(function(err) {
			console.log('erro cargando máis persoas: ' + err.error);
		})
		.finally(function() {
			$scope.cargandoMais = false;
			$timeout(function() { $animate.enabled(true); }, 500);
		});
	};					
});
