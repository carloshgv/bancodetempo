app.controller('anunciosController', function($scope, $rootScope, $mdToast, $timeout, $interval, $animate, $compile, anunciosService, eventos) {
	// inicialización
	$scope.cargando = true;
	$scope.ultimaActualizacion = new Date().getTime();

	anunciosService.getAnuncios()
	.then(function(anuncios) {
		$scope.anuncios = anuncios;
	})
	.catch(function(err) {
		console.log('erro cargando anuncios: ' + err);
	})
	.finally(function() {
		$scope.cargando = false;
	});

	anunciosService.total()
	.then(function(total) {
		$scope.totalAnuncios = total;
	});

	// métodos
	$scope.editabel = function(anuncio) {
		return $rootScope.session && anuncio.anunciante.id == $rootScope.session.id;
	}

	$scope.cargarMaisAnuncios = function() {
		if($scope.anuncios.length == $scope.totalAnuncios)
			return;
		$scope.cargandoMais = true;
		$animate.enabled(false);
		anunciosService.getAnuncios($scope.anuncios.length)
		.then(function(anuncios) {
			$scope.anuncios = $scope.anuncios.concat(anuncios);
		})
		.catch(function(err) {
			console.log('erro cargando máis anuncios: ' + err.error);
		})
		.finally(function() {
			$scope.cargandoMais = false;
			$timeout(function() { $animate.enabled(true); }, 500);
		});
	};

	$scope.obterNovos = function() {
		anunciosService.novosDesde(new Date($scope.anuncios[0].dateCreated).getTime())
		.then(function(novos) {
			novos.forEach(function(novo) {
				$scope.anuncios.unshift(novo);
			});
		});
	};

	$scope.obterEditados = function() {
		if($scope.anuncios && $scope.anuncios.length > 0) {
			var ids = $scope.anuncios.map(function(a) { return a.id; });
			anunciosService.actualizadosDesde($scope.ultimaActualizacion, ids)
			.then(function(actualizados) {
				actualizados.forEach(function(actualizacion) {
					$scope.anuncios.forEach(function(anuncio) { 
						if(anuncio.id == actualizacion.id)
							anuncio.texto = actualizacion.texto;
					});
				});
				$scope.ultimaActualizacion = new Date().getTime();
			});
		}
	}

	// polling
	$interval($scope.obterNovos, 60000); // actualizar cada minuto
	//$interval($scope.obterEditados, 60000); // actualizar cada minuto
});
