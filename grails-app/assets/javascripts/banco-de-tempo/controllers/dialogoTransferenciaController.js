app.controller('dialogoTransferenciaController', function($mdDialog, $scope, $rootScope, 
                                                          persoasService, transferenciasService, para) {
	$scope.buscando = '';
	$scope.transferencia = {};
	$scope.session = $rootScope.session;
	var nomes = [];
	persoasService.buscarNomesComo()
	.then(function(n) {
		nomes = n;
	});
	if(para) {
		$scope.fixed = true;
		$scope.transferencia = { para: para };
	}

	$scope.buscarEnPersoas = function(busqueda) {
		var minusculas = angular.lowercase(busqueda);
		return nomes.filter(function(n) {
			var nome = angular.lowercase(n.nome);
			return nome.indexOf(minusculas) == 0; 
		});
	}

	$scope.aceptar = function(transferencia) {
		// de -> para -> cantidade -> concepto
		transferenciasService.transferir($scope.session.conta, transferencia.para.conta,
		                                 transferencia.horas, transferencia.minutos, transferencia.concepto)
		.then(function() {
			$mdDialog.hide();
		})
		.catch(function(err) {
			$scope.transferenciaForm.mensaxeErro = err.error;	
		});
	};

	$scope.cancelar = function() {
		$mdDialog.cancel();
	};
});