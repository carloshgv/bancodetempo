app.controller('userBarController', function($scope, $rootScope, $interval, transferenciasService, anunciosService, eventos) {
	$scope.ultimoAnuncio = {
		texto: '',
	};

	var obterInfo = function() {
		anunciosService.ultimoDe($rootScope.session.id)
		.then(function(anuncio) {
			$scope.ultimoAnuncio = anuncio;
			$scope.ultimoAnuncio.dateCreated = new Date(anuncio.dateCreated);
		})
		.catch(function() {
			$scope.ultimoAnuncio.texto = 'Non tes anuncios';
		})

		transferenciasService.ultimasDe($rootScope.session.id)
		.then(function(transferencias) {
			if(transferencias.length > 0)
				$scope.ultimasTransferencias = transferencias;
		});
	};
	obterInfo();
	$scope.$on(eventos.perfilCambiado, obterInfo);

	$scope.facerTransferencia = function($event) {
		transferenciasService.pedirDatos($event)
		.then(function(para) {
			$rootScope.$broadcast(eventos.perfilCambiado);
		});
	};
});