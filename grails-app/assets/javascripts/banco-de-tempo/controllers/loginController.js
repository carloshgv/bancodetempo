app.controller('loginController', function($scope, $rootScope, sessionService, eventos) {
	$scope.login = function() {
		sessionService.login($scope.email, $scope.password, $scope.manterSesion)
		.then(function(data) {
			$rootScope.$broadcast(eventos.iniciarSesion, data);
		})
		.catch(function(err) {
			$scope.loginForm.mensaxeErro = err;
			$scope.loginForm.$setValidity('autenticada', false);
		});
	}
});