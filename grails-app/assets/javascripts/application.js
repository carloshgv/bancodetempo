// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery/dist/jquery.js
//= require angular/angular.js
//= require angular-ui-router/release/angular-ui-router.js
//= require angular-animate/angular-animate.js
//= require angular-aria/angular-aria.js
//= require angular-material/angular-material.js
//= require angular-messages/angular-messages.js
//= require spin.js/spin.js
//= require angular-spinner/angular-spinner.js
//= require_self
//= require_tree .

var app = angular.module('bancoDeTempo', [ 'ui.router', 'ngMaterial', 'ngMessages', 'ngAnimate', 'angularSpinner' ]);
app.value('eventos', {
	iniciarSesion: 'iniciarSesion',
	perfilCambiado: 'perfilCambiado',
	finScroll: 'finScroll',
	inicioApp: 'inicioApp',
	editarAnuncio: 'editarAnuncio',
	actualizarTimestamps: 'actualizarTimestamps'
});
app.constant('apiRoot', $('#apiRoot').attr('href'));
app.run(function(sessionService, $rootScope, $q, eventos) {
	$rootScope.loading = true;

	var comprobandoSesion = sessionService.obterPerfil().then(
		function(data) {
			$rootScope.session = data;
		}
	);

	$q.all([ comprobandoSesion ]).finally(function() {
		$rootScope.loading = false;
		$rootScope.$broadcast(eventos.inicioApp);
	});
});
app.service('nl2br', function() {
	return function(string) {
		return string.replace(/\n/gm, '<br/>');
	}
});
app.filter('ellipsisHtml', function ($sce, nl2br) {
    return function (text, length) {
    	var apparentLength = 0, lineLength = 0, i = 0, newLines = 0;
    	while(apparentLength <= length && i < text.length) {
    		if(text[i] === '\n') {
    			apparentLength += 50 - lineLength%50; // 50 é a anchura do sidebar en caracteres. debería calcularse
    			lineLength = 0;
    			newLines++;
    		} else {
    			apparentLength++;
	    		lineLength++;
    		}
    		i++;
    	}
        if (apparentLength > length) {
            return $sce.trustAsHtml(nl2br(text.substr(0, i-newLines) + '&hellip;'));
        }
        return $sce.trustAsHtml(nl2br(text));
    }
});
