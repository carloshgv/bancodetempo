import groovy.xml.StreamingMarkupBuilder

eventCompileStart = {
	def output = new StringBuffer()
	def error = new StringBuffer()
	def commands = [
		[ message: 'Installing dependencies', cmd: 'bower install' ],
		[ message: 'Compiling admin.scss', cmd: 'sass -I grails-app/assets/stylesheets grails-app/assets/stylesheets/admin/admin.scss grails-app/assets/stylesheets/admin/all.css' ],
		[ message: 'Compiling application.scss', 
		      cmd: 'sass -I grails-app/assets/stylesheets grails-app/assets/stylesheets/application.scss grails-app/assets/stylesheets/application.css' ]
	]

	def prefix = System.properties['os.name'].toLowerCase().contains('windows') ? 'cmd /c' : ''

	commands.each {
		println "| ${it.message}"
		proc = "${prefix} ${it.cmd}".execute()
		proc.consumeProcessOutput(output, error)
		proc.waitFor()
		println output
		if(proc.exitValue() != 0) {
			println '| Error'
			println error
		}
	}
}

