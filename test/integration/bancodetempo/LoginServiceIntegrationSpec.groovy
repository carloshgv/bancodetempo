package bancodetempo

import grails.test.spock.IntegrationSpec

class LoginServiceIntegrationSpec extends IntegrationSpec {
	def loginService
	def bcryptService

	void "test login service"() {
		given:
		def persoaTest = new Persoa(
			nome: 'Boos',
			email: 'boos@hotmail.com',
			password: bcryptService.hashPassword('boos'),
			description: 'asldkfjlkasdjflk'
		)
		persoaTest.save(failOnError: true)

		when:
		def persoa1 = loginService.login('boos@hotmail.com', 'boos')
		def persoa2 = loginService.login('boos@hotmail.com', 'abcdefg')
		def persoa3 = loginService.login('asdflkj@alsdfkj.com', 'boos')

		then:
		persoa1 != null
		persoa1.nome == 'Boos'
		persoa2 == null
		persoa3 == null

		cleanup:
		persoaTest.delete()
	}
}
