package bancodetempo

import grails.test.spock.IntegrationSpec
import bancodetempo.api.AuthController
import grails.converters.JSON

class AuthControllerIntegrationSpec extends IntegrationSpec {
	def bcryptService

	void "test login controller"() {
		given:
		def persoaTest = new Persoa(
			nome: 'Boos',
			email: 'boos@hotmail.com',
			password: bcryptService.hashPassword('boos'),
			description: 'asldkfjlkasdjflk'
		)
		persoaTest.save(failOnError: true)
		def controller = new AuthController()

		when:
		controller.request.method = method
		controller.request.json = [ email: email, password: pass ] as JSON
		controller.login()

		then:
		controller.response.status == status

		cleanup:
		persoaTest.delete()

		where:
		method << [ 'POST', 'GET', 'POST', 'POST' ]
		email << [ 'boos@hotmail.com', 'boos@hotmail.com', 'safd@asdf.com', 'boos@hotmail.com' ]
		pass << [ 'boos', 'boos', 'boos', 'sadf' ]
		status << [ 200, 405, 401, 401 ]
	}

	void "test cookie sesion"() {
		given:
		def persoaTest = new Persoa(
			nome: 'Boos',
			email: 'boos@hotmail.com',
			password: bcryptService.hashPassword('boos'),
			description: 'asldkfjlkasdjflk'
		)
		persoaTest.save(failOnError: true)
		def controller = new AuthController()

		when:
		controller.request.method = 'POST'
		controller.request.json = [
			email: 'boos@hotmail.com',
			password: 'boos',
			manterSesion: false
		]
		controller.login()

		then:
		controller.response.getCookie('sessionUUID') == null

		cleanup:
		persoaTest.delete()
	}
}
